const REASONS = [
  'INTEMPERANCE & BUSINESS TROUBLE',
  'KICKED IN THE HEAD BY A HORSE',
  'HEREDITARY PREDISPOSITION',
  'ILL TREATMENT BY HUSBAND',
  'IMAGINARY FEMALE TROUBLE',
  'HYSTERIA',
  'IMMORAL LIFE',
  'IMPRISONMENT',
  'JEALOUSY AND RELIGION',
  'LAZINESS',
  'MARRIAGE OF SON',
  'MASTURBATION & SYPHILIS',
  'MASTURBATION FOR 30 YEARS',
  'MEDICINE TO PREVENT CONCEPTION',
  'MENSTRUAL DERANGED',
  'MENTAL EXCITEMENT',
  'NOVEL READING',
  'NYMPHOMANIA',
  'OPIUM HABIT',
  'OVER ACTION OF THE MIND',
  'OVER STUDY OF RELIGION',
  'OVER TAXING MENTAL POWERS',
  'PARENTS WERE COUSINS',
  'PERIODICAL FITS.',
  'TOBACCO & MASTURBATION',
  'POLITICAL EXCITEMENT',
  'POLITICS',
  'RELIGIOUS ENTHUSIASM',
  'FEVER AND LOSS OF LAW SUIT',
  'FITS AND DESERTION OF HUSBAND',
  'ASTHMA',
  'BAD COMPANY',
  'BAD HABITS & POLITICAL EXCITEMENT',
  'BAD WHISKEY',
  'BLOODY FLUX',
  'BRAIN FEVER',
  'BUSINESS NERVES',
  'CARBONIC ACID GAS',
  'CONGESTION OF BRAIN',
  'DEATH OF SONS IN WAR',
  'DECOYED INTO THE ARMY',
  'DERANGED MASTURBATION',
  'DESERTION BY HUSBAND',
  'DISSOLUTE HABITS',
  'DOMESTIC AFFLICTION',
  'DOMESTIC TROUBLE',
  'DROPSY',
  'EGOTISM',
  'EPILEPTIC FITS',
  'EXCESSIVE SEXUAL ABUSE',
  'EXCITEMENT AS OFFICER',
  'EXPOSURE AND HEREDITARY',
  'EXPOSURE AND QUACKERY',
  'EXPOSURE IN ARMY',
  'FEVER AND JEALOUSY',
  'FIGHTING FIRE',
  'SUPPRESSED MASTURBATION',
  'SUPPRESSION OF MENSES',
  'THE WAR',
  'TIME OF LIFE',
  'UTERINE DERANGEMENT',
  'VENEREAL EXCESSES',
  'VICIOUS VICES',
  'WOMEN TROUBLE',
  'SUPERSTITION',
  'SHOOTING OF DAUGHTER',
  'SMALL POX',
  'SNUFF EATING FOR 2 YEARS',
  'SPINAL IRRITATION',
  'GATHERING IN THE HEAD',
  'GREEDINESS',
  'GRIEF',
  'GUNSHOT WOUND',
  'HARD STUDY',
  'RUMOR OF HUSBAND MURDER',
  'SALVATION ARMY',
  'SCARLATINA',
  'SEDUCTION & DISAPPOINTMENT',
  'SELF ABUSE',
  'SEXUAL ABUSE & STIMULANTS',
  'SEXUAL DERANGEMENT',
  'FALSE CONFINEMENT',
  'FEEBLENESS OF INTELLECT',
  'FELL FROM HORSE IN WAR',
  'FEMALE DISEASE',
  'DISSIPATION OF NERVES',
];

// Random integer in the [lo, hi) range.
const randInt = ([lo, hi]: [number, number]): number =>
  lo + Math.floor(Math.random() * (hi - lo));

// Fisher-Yates, heck yeah.
const shuffle = <T>(ary: T[]): T[] => {
  for (let i = ary.length - 1; i > 0; i -= 1) {
    const j = randInt([0, i + 1]);
    [ary[i], ary[j]] = [ary[j], ary[i]];
  }
  return ary;
};

const dewIt = (): void => {
  const shuffled = shuffle(REASONS);
  const grid = document.createElement('div');
  grid.id = 'grid';

  for (let i = 0; i < 25; i += 1) {
    const isFreeSpace = i === 12;

    const cell = document.createElement('div');
    cell.classList.add('cell');
    cell.textContent = isFreeSpace ? 'FREE SPACE' : shuffled[i];
    if (isFreeSpace) {
      cell.classList.add('selected');
    }

    cell.onclick = () => {
      if (isFreeSpace) {
        return;
      }

      if (cell.classList.contains('selected')) {
        cell.classList.remove('selected');
      } else {
        cell.classList.add('selected');
      }
    };
    grid.appendChild(cell);
  }
  document.body.appendChild(grid);
};

document.body.onload = dewIt;
